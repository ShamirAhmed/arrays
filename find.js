function find(elements, callback){

    if(elements === undefined || callback === undefined || Array.isArray(elements) === false || typeof callback !== 'function')
        return [];
    else{
        for(let index=0; index<elements.length; index++){
            if(callback(elements[index]))
                return elements[index];
        }
        return undefined;
    }

}

module.exports = find;