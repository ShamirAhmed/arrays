function flatten(elements, depth = 1){

    if(elements === undefined || Array.isArray(elements) === false)
        return [];
    else{
        let newFlatten = [];
        for(let index=0; index<elements.length; index++){
            if(Array.isArray(elements[index]) === true && depth > 0){
                this.depth++;
                newFlatten = newFlatten.concat(flatten(elements[index],depth-1));
            }
            else if(elements[index] === undefined)
                continue;
            else{
                newFlatten.push(elements[index]);
            }
        }
        return newFlatten;
    }

}

module.exports = flatten;