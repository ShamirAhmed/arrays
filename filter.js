function filter(elements, callback){

    if(elements === undefined || callback === undefined || Array.isArray(elements) === false || typeof callback !== 'function')
        return [];
    else{
        let filteredElements = [];
        for(let index=0; index<elements.length; index++){
            if(callback(elements[index], index, elements) === true)
                filteredElements.push(elements[index]);
        }
        return filteredElements;
    }

}

module.exports = filter;