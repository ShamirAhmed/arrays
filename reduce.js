function reduce(elements, callback, startingValue){

    if(elements === undefined || callback === undefined || Array.isArray(elements) === false || typeof callback !== 'function')
        return [];
    else{
        let accumulator = startingValue || elements[0];
        for(let index=startingValue === undefined ? 1 : 0; index < elements.length; index++){
            accumulator = callback(accumulator, elements[index], index, elements)
        }
        return accumulator
    }
}

module.exports = reduce;