function each(elements, callback){

    if(elements === undefined || callback === undefined || Array.isArray(elements) === false || typeof callback !== 'function')
        return [];
    else{
        for(let index=0; index<elements.length; index++){
            callback(elements[index], index, elements)
        }
    }

}

module.exports = each;