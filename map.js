function map(elements, callback){
    let newElements = [];

    if(elements === undefined || callback === undefined || Array.isArray(elements) === false || typeof callback !== 'function')
        return [];
    else{
        for(let index=0; index<elements.length; index++){
            let callbackReturnValue = callback(elements[index], index, elements);
            newElements.push(callbackReturnValue);
        }
        return newElements;
    }

}

module.exports = map;